const { kebabCase, PascalCase, fileName } = require("./helpers.js");
const { CleanUpSVG } = require("./helpers");

const BUILD = {};

BUILD.SPEC = async (fileData, options) => {
  const component = PascalCase(
    (options.prefix ? options.prefix : "illustration-") + fileName(fileData.name)
  );
  const file = `import { ${component} } from './${kebabCase(
    fileName(fileData.name)
  )}';

    describe('${options.prefix ? options.prefix : "illustration-"}${kebabCase(
    fileName(fileData.name)
  )}', () => {
        it('builds', () => {
        expect(new ${component}()).toBeTruthy();
        });
    });
    `;
  return file;
};

BUILD.E2E = async (fileData, options) => {
  const file = `import { newE2EPage } from '@stencil/core/testing';

    describe('illustration-${kebabCase(fileName(fileData.name))}', () => {
        it('renders', async () => {
        const page = await newE2EPage();
        await page.setContent('<${
          options.prefix ? options.prefix : "illustration-"
        }${kebabCase(fileName(fileData.name))}></${
    options.prefix ? options.prefix : "illustration-"
  }${kebabCase(fileName(fileData.name))}>');
        await page.waitForChanges();
    
        const element = await page.find('${
          options.prefix ? options.prefix : "illustration-"
        }${kebabCase(fileName(fileData.name))}');
        expect(element).toHaveClass('hydrated');
        });
    });
`;
  return file;
};

BUILD.CSS = async () => {
  const file = `
  svg {
  display: block;
  margin: auto;
  width: var(--cf-illustration-width);
  height: var(--cf-illustration-height);
  }
`;
  return file;
};

BUILD.TSX = async (fileData, options) => {
  const data = CleanUpSVG(fileData.data)
  const currentFileName = `${
    options.prefix ? options.prefix : "illustration-"
  }${kebabCase(fileName(fileData.name))}`;
  const file = `import { Component, h, Host } from '@stencil/core';
    
@Component({
  tag: '${currentFileName}',
  styleUrl: '${kebabCase(fileName(fileData.name))}.css',
  shadow: true
})
export class ${PascalCase(currentFileName)} {
  render() {
    return (
        <Host>
            ${data}
        </Host>
    );
  }
}`;
    return file;
};

BUILD.STORYBOOK = async (fileData, options) => {
  const currentFileName = `${
    options.prefix ? options.prefix : "illustration-"
  }${kebabCase(fileName(fileData.name))}`;

  const file = `
    import { html } from 'lit-html';

    export default {
      title: 'Illustrations/${kebabCase(fileName(fileData.name))}',
      component: '${currentFileName}',
      argTypes: {}
    };

    export const Default = () => html\`<${currentFileName}></${currentFileName}>\`;
    `;
  return file;
};

BUILD.DOCS = async (files, options) => {
  const elements = files
    .map((x) => `<cf-illustration-${kebabCase(fileName(x.name))} />`)
    .join(",");
  const file = `
import { Meta, Preview } from '@storybook/addon-docs/blocks';

<Meta title="Illustrations docs" />

<div>
  <Preview>
  {[${elements}].map(el => (
    <div key={el.key}>
      {el}
    </div>
  ))}
  </Preview>
</div>
    `;
  return file;
};

module.exports = BUILD;
